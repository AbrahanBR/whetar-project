import React, { useState } from 'react';
import Header from './components/header';
import { Forecast } from './services/types';
import { getDailyforecast } from "./services/weather-station"

const App = () => {
  let [forecast, setForecast]= useState<Forecast>();
  return (
  <>
    <Header onSearch={async (city) =>{
      if(city && city  !== forecast?.city.name){
        setForecast(await getDailyforecast(city))
      }
    }}/>
    <main>
      <div>
        
        {forecast && JSON.stringify(forecast)}
      </div>
    </main>
  </>
  );
}

export default App;

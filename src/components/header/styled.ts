import styled from "styled-components";

export const Container = styled.header`
    display: flex;
    justify-content: center;
    align-items: center;

    background-color: #5865f2;
    color: #fff;
`;
export const InputDiv = styled.div `
    display: flex;
    margin-left: 5px;
    height: 40px;
`;
export const SearchInput = styled.input`
    border: none;
    outline: none;
    padding-left: 15px;
    border-top-left-radius: 50px;
    border-bottom-left-radius: 50px;
`;
export const SearchButton = styled.button`
    border: none;
    border-top-right-radius: 50px;
    border-bottom-right-radius: 50px;
    color: #50afe1;
    background-color: #fff;
    font-size: initial;
    font-weight: bold;

    &:hover{
        cursor: pointer;
        background-color: #50afe1;
        color: #fff;
    }
`;
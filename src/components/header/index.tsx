import React, { useState } from 'react';
import { noop } from "lodash";
import { Props } from "./types";
import { Container, InputDiv, SearchButton, SearchInput } from './styled';
 
const Header = (props: Props) => {
    const [city, setCity] = useState("")

    return (
    <>
        <Container>
             <h1>Weather Forecast</h1>
            <InputDiv>
                <SearchInput type="string" value={city} onChange={(e) => setCity(e.target.value)}/>
                <SearchButton onClick={() => props.onSearch(city)}>Search</SearchButton>
            </InputDiv>
        </Container>
    </>
    )
}

Header.defaultProps = {
    onSearch : noop
}
export default Header;
